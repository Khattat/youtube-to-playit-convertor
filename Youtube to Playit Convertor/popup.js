// Copyright (c) 2015 Khattat Author. All rights reserved.


function youtube_click(e) {
  chrome.tabs.executeScript(null,
      {file:"youtube_script.js"},
	  function(results){ /*console.log(results);*/ }
  );
  window.close();
}

function playit_click(e) {
  chrome.tabs.executeScript(null,
      {file:"playit_script.js"},
	  function(results){ /*console.log(results);*/ }
  );
  window.close();
}

document.addEventListener('DOMContentLoaded', function () {
  chrome.tabs.executeScript(null, {file:"jquery.js"}, function(results){ /*console.log(results);*/ });
  
  var divs = document.querySelectorAll('div');
  divs[0].addEventListener('click', youtube_click);
  divs[1].addEventListener('click', playit_click);
});
